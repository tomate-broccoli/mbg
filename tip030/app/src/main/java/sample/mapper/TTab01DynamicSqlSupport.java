package sample.mapper;

import jakarta.annotation.Generated;
import java.sql.JDBCType;
import java.util.Date;
import org.mybatis.dynamic.sql.AliasableSqlTable;
import org.mybatis.dynamic.sql.SqlColumn;

public final class TTab01DynamicSqlSupport {
    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final TTab01 TTab01 = new TTab01();

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Short> cNum01 = TTab01.cNum01;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<String> cChr01 = TTab01.cChr01;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Date> cDat01 = TTab01.cDat01;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final class TTab01 extends AliasableSqlTable<TTab01> {
        public final SqlColumn<Short> cNum01 = column("C_NUM01", JDBCType.NUMERIC);

        public final SqlColumn<String> cChr01 = column("C_CHR01", JDBCType.VARCHAR);

        public final SqlColumn<Date> cDat01 = column("C_DAT01", JDBCType.TIMESTAMP);

        public TTab01() {
            super("SCOTT.T_TAB01", TTab01::new);
        }
    }
}