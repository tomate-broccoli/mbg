package sample.model;

import jakarta.annotation.Generated;
import java.util.Date;

public class MTab01 {
    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Short num01;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String chr01;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Date dat01;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Short getNum01() {
        return num01;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setNum01(Short num01) {
        this.num01 = num01;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getChr01() {
        return chr01;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setChr01(String chr01) {
        this.chr01 = chr01;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Date getDat01() {
        return dat01;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setDat01(Date dat01) {
        this.dat01 = dat01;
    }
}