# Memo.md

## Oracleの起動

$ docker login registry.gitlab.com
$ docker pull registry.gitlab.com/tomate-broccoli/gitpod_tip011
$ docker run -d --env-file ./oracle.env --name ORCL2 -p 1521:1521 registry.gitlab.com/tomate-broccoli/gitpod_tip011

### Oracle再起動

$ docker start ORCl2

## テーブル作成

$ docker exec -it ORCL2 sqlplus system/oracle21C3@ORCL

select name, open_mode from v$pdbs;
alter session set container = MYORCL ;

create user scott identified by tiger account unlock ;
grant connect, resource to scott ;
alter user scott quota UNLIMITED on users ;
exit;

$ docker exec -it ORCL2 sqlplus scott/tiger@MYORCL

create table t_tab01 ( c_num01 number, c_chr01 varchar2(50), c_dat01 date) ;
insert into t_tab01 values(1, 'abc', sysdate);
insert into t_tab01 values(2, 'xyz', sysdate);
commit;
select * from t_tab01;
exit;
