package sample;

import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;

public class Tip030Util {

    public static String getGetterMethodName(String property, FullyQualifiedJavaType fullyQualifiedJavaType){
        System.out.printf("** Tip030Util.getGetterMethodNam: property=[%s].\n", property);

        StringBuilder sb = new StringBuilder();
    	sb.append(property);

        // if (Character.isLowerCase(sb.charAt(0)) && (sb.length() == 1 || !Character.isUpperCase(sb.charAt(1)))) {    
        if (Character.isLowerCase(sb.charAt(0))){
            sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
        }

        if (fullyQualifiedJavaType.equals(FullyQualifiedJavaType.getBooleanPrimitiveInstance())) {
            sb.insert(0, "is"); //$NON-NLS-1$
        } else {
            sb.insert(0, "get"); //$NON-NLS-1$
        }

        String ret = sb.toString();
        System.out.printf("** result=[%s].\n", ret);
        return ret;
    }    

}
