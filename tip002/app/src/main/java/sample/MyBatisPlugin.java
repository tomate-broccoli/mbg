package sample;

import java.util.List;

import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.TopLevelClass;

public class MyBatisPlugin extends PluginAdapter {

    @Override
    public boolean validate(List<String> list) {
        return true;
    }

    /*
     * Daoクラスの自動生成をコントロールするメソッド
     */
    @Override
    public boolean modelBaseRecordClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {

        // Lombok機能をimportする
        topLevelClass.addImportedType("lombok.Data");
        // アノテーション追加
        topLevelClass.addAnnotation("@Data");

        return true;
    }

    /*
     * DtoクラスのSetterメソッドをコントロールするメソッド
     */
    @Override
    public boolean modelSetterMethodGenerated(Method method, TopLevelClass topLevelClass,
            IntrospectedColumn introspectedColumn, IntrospectedTable introspectedTable, ModelClassType modelClassType) {

        // DtoクラスのSetterメソッドを自動生成しないようにする
        return false;
    }

    /*
     * DtoクラスのGetterメソッドをコントロールするメソッド
     */
    @Override
    public boolean modelGetterMethodGenerated(Method method, TopLevelClass topLevelClass,
            IntrospectedColumn introspectedColumn, IntrospectedTable introspectedTable, ModelClassType modelClassType) {

        // DtoクラスのGetterメソッドを自動生成しないようにする
        return false;
    }

}

