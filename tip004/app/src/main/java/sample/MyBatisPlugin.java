package sample;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;

public class MyBatisPlugin extends PluginAdapter {

    private Properties props;
 
    @Override
    public void setProperties(Properties properties) {
        this.props = properties;
    }

    @Override
    public boolean validate(List<String> list) {
        return true;
    }

    @Override
    public void initialized(IntrospectedTable table) {
        List<String> list = new ArrayList(Arrays.asList(table.getMyBatisDynamicSqlSupportType().split("\\.")));
        String className = list.get(list.size()-1);
        list.remove(list.size()-1);
        String nowPackageName = String.join(".", list);
        List<String> set = new ArrayList(this.props.stringPropertyNames());
        if(set.contains(nowPackageName)){
            String newPackageName = this.props.getProperty(nowPackageName);
            table.setMyBatisDynamicSqlSupportType(newPackageName+"."+className);
        }
    }

}

