package sample.mapper;

import static sample.mapper.TTab01DynamicSqlSupport.*;

import jakarta.annotation.Generated;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.dynamic.sql.BasicColumn;
import org.mybatis.dynamic.sql.delete.DeleteDSLCompleter;
import org.mybatis.dynamic.sql.select.CountDSLCompleter;
import org.mybatis.dynamic.sql.select.SelectDSLCompleter;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.UpdateDSL;
import org.mybatis.dynamic.sql.update.UpdateDSLCompleter;
import org.mybatis.dynamic.sql.update.UpdateModel;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import org.mybatis.dynamic.sql.util.mybatis3.CommonCountMapper;
import org.mybatis.dynamic.sql.util.mybatis3.CommonDeleteMapper;
import org.mybatis.dynamic.sql.util.mybatis3.CommonInsertMapper;
import org.mybatis.dynamic.sql.util.mybatis3.CommonUpdateMapper;
import org.mybatis.dynamic.sql.util.mybatis3.MyBatis3Utils;
import sample.model.TTab01;

@Mapper
public interface TTab01Mapper extends CommonCountMapper, CommonDeleteMapper, CommonInsertMapper<TTab01>, CommonUpdateMapper {
    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    BasicColumn[] selectList = BasicColumn.columnList(cNum01, cChr01, cDat01);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @Results(id="TTab01Result", value = {
        @Result(column="C_NUM01", property="cNum01", jdbcType=JdbcType.NUMERIC),
        @Result(column="C_CHR01", property="cChr01", jdbcType=JdbcType.VARCHAR),
        @Result(column="C_DAT01", property="cDat01", jdbcType=JdbcType.TIMESTAMP)
    })
    List<TTab01> selectMany(SelectStatementProvider selectStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @ResultMap("TTab01Result")
    Optional<TTab01> selectOne(SelectStatementProvider selectStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default long count(CountDSLCompleter completer) {
        return MyBatis3Utils.countFrom(this::count, TTab01, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int delete(DeleteDSLCompleter completer) {
        return MyBatis3Utils.deleteFrom(this::delete, TTab01, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int insert(TTab01 row) {
        return MyBatis3Utils.insert(this::insert, row, TTab01, c ->
            c.map(cNum01).toProperty("cNum01")
            .map(cChr01).toProperty("cChr01")
            .map(cDat01).toProperty("cDat01")
        );
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int insertMultiple(Collection<TTab01> records) {
        return MyBatis3Utils.insertMultiple(this::insertMultiple, records, TTab01, c ->
            c.map(cNum01).toProperty("cNum01")
            .map(cChr01).toProperty("cChr01")
            .map(cDat01).toProperty("cDat01")
        );
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int insertSelective(TTab01 row) {
        return MyBatis3Utils.insert(this::insert, row, TTab01, c ->
            c.map(cNum01).toPropertyWhenPresent("cNum01", row::getCNum01)
            .map(cChr01).toPropertyWhenPresent("cChr01", row::getCChr01)
            .map(cDat01).toPropertyWhenPresent("cDat01", row::getCDat01)
        );
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default Optional<TTab01> selectOne(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectOne(this::selectOne, selectList, TTab01, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default List<TTab01> select(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectList(this::selectMany, selectList, TTab01, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default List<TTab01> selectDistinct(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectDistinct(this::selectMany, selectList, TTab01, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int update(UpdateDSLCompleter completer) {
        return MyBatis3Utils.update(this::update, TTab01, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    static UpdateDSL<UpdateModel> updateAllColumns(TTab01 row, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(cNum01).equalTo(row::getCNum01)
                .set(cChr01).equalTo(row::getCChr01)
                .set(cDat01).equalTo(row::getCDat01);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    static UpdateDSL<UpdateModel> updateSelectiveColumns(TTab01 row, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(cNum01).equalToWhenPresent(row::getCNum01)
                .set(cChr01).equalToWhenPresent(row::getCChr01)
                .set(cDat01).equalToWhenPresent(row::getCDat01);
    }
}