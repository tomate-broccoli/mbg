package sample.model;

import jakarta.annotation.Generated;
import java.util.Date;
import lombok.Data;

@Data
public class MTab01 {
    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Short num01;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String chr01;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Date dat01;
}