package sample.mapper;

import static sample.mapper.MTab01DynamicSqlSupport.*;

import jakarta.annotation.Generated;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.dynamic.sql.BasicColumn;
import org.mybatis.dynamic.sql.delete.DeleteDSLCompleter;
import org.mybatis.dynamic.sql.select.CountDSLCompleter;
import org.mybatis.dynamic.sql.select.SelectDSLCompleter;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.UpdateDSL;
import org.mybatis.dynamic.sql.update.UpdateDSLCompleter;
import org.mybatis.dynamic.sql.update.UpdateModel;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import org.mybatis.dynamic.sql.util.mybatis3.CommonCountMapper;
import org.mybatis.dynamic.sql.util.mybatis3.CommonDeleteMapper;
import org.mybatis.dynamic.sql.util.mybatis3.CommonInsertMapper;
import org.mybatis.dynamic.sql.util.mybatis3.CommonUpdateMapper;
import org.mybatis.dynamic.sql.util.mybatis3.MyBatis3Utils;
import sample.model.MTab01;

@Mapper
public interface MTab01Mapper extends CommonCountMapper, CommonDeleteMapper, CommonInsertMapper<MTab01>, CommonUpdateMapper {
    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    BasicColumn[] selectList = BasicColumn.columnList(num01, chr01, dat01);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @Results(id="MTab01Result", value = {
        @Result(column="NUM01", property="num01", jdbcType=JdbcType.NUMERIC),
        @Result(column="CHR01", property="chr01", jdbcType=JdbcType.VARCHAR),
        @Result(column="DAT01", property="dat01", jdbcType=JdbcType.TIMESTAMP)
    })
    List<MTab01> selectMany(SelectStatementProvider selectStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @ResultMap("MTab01Result")
    Optional<MTab01> selectOne(SelectStatementProvider selectStatement);

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default long count(CountDSLCompleter completer) {
        return MyBatis3Utils.countFrom(this::count, MTab01, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int delete(DeleteDSLCompleter completer) {
        return MyBatis3Utils.deleteFrom(this::delete, MTab01, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int insert(MTab01 row) {
        return MyBatis3Utils.insert(this::insert, row, MTab01, c ->
            c.map(num01).toProperty("num01")
            .map(chr01).toProperty("chr01")
            .map(dat01).toProperty("dat01")
        );
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int insertMultiple(Collection<MTab01> records) {
        return MyBatis3Utils.insertMultiple(this::insertMultiple, records, MTab01, c ->
            c.map(num01).toProperty("num01")
            .map(chr01).toProperty("chr01")
            .map(dat01).toProperty("dat01")
        );
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int insertSelective(MTab01 row) {
        return MyBatis3Utils.insert(this::insert, row, MTab01, c ->
            c.map(num01).toPropertyWhenPresent("num01", row::getNum01)
            .map(chr01).toPropertyWhenPresent("chr01", row::getChr01)
            .map(dat01).toPropertyWhenPresent("dat01", row::getDat01)
        );
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default Optional<MTab01> selectOne(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectOne(this::selectOne, selectList, MTab01, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default List<MTab01> select(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectList(this::selectMany, selectList, MTab01, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default List<MTab01> selectDistinct(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectDistinct(this::selectMany, selectList, MTab01, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    default int update(UpdateDSLCompleter completer) {
        return MyBatis3Utils.update(this::update, MTab01, completer);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    static UpdateDSL<UpdateModel> updateAllColumns(MTab01 row, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(num01).equalTo(row::getNum01)
                .set(chr01).equalTo(row::getChr01)
                .set(dat01).equalTo(row::getDat01);
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    static UpdateDSL<UpdateModel> updateSelectiveColumns(MTab01 row, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(num01).equalToWhenPresent(row::getNum01)
                .set(chr01).equalToWhenPresent(row::getChr01)
                .set(dat01).equalToWhenPresent(row::getDat01);
    }
}