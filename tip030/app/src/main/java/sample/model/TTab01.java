package sample.model;

import jakarta.annotation.Generated;
import java.util.Date;

public class TTab01 {
    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Short cNum01;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String cChr01;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Date cDat01;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Short getCNum01() {
        return cNum01;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setcNum01(Short cNum01) {
        this.cNum01 = cNum01;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getCChr01() {
        return cChr01;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setcChr01(String cChr01) {
        this.cChr01 = cChr01;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Date getCDat01() {
        return cDat01;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setcDat01(Date cDat01) {
        this.cDat01 = cDat01;
    }
}