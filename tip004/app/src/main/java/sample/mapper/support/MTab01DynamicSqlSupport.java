package sample.mapper.support;

import jakarta.annotation.Generated;
import java.sql.JDBCType;
import java.util.Date;
import org.mybatis.dynamic.sql.AliasableSqlTable;
import org.mybatis.dynamic.sql.SqlColumn;

public final class MTab01DynamicSqlSupport {
    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final MTab01 MTab01 = new MTab01();

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Short> num01 = MTab01.num01;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<String> chr01 = MTab01.chr01;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final SqlColumn<Date> dat01 = MTab01.dat01;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public static final class MTab01 extends AliasableSqlTable<MTab01> {
        public final SqlColumn<Short> num01 = column("NUM01", JDBCType.NUMERIC);

        public final SqlColumn<String> chr01 = column("CHR01", JDBCType.VARCHAR);

        public final SqlColumn<Date> dat01 = column("DAT01", JDBCType.TIMESTAMP);

        public MTab01() {
            super("M_TAB_01", MTab01::new);
        }
    }
}